﻿namespace Lab1._3_SparseMatrix
{
    public class SparseMatrixElement
    {
        public SparseMatrixElement(int value, int row, int column)
        {
            Value = value;
            Row = row;
            Column = column;
        }

        public int Value { get; }
        public int Row { get; }
        public int Column { get; }
    }
}
