﻿using System;

namespace Lab1._3_SparseMatrix
{
    public class SparseMatrix
    {
        public SparseMatrix(int rows, int columns, double sparsity, Random rnd)
        {
            Rows = rows; Columns = columns;

            int quantity = Convert.ToInt32(rows * columns * sparsity);

            SparseMatrixElements = new SparseMatrixElement[quantity];
            int index = 0;
            while (quantity != 0)
            {
                int row = rnd.Next(0, Rows);
                int column = rnd.Next(0, Columns);
                if (TakeValueFromMatrix(row, column) == 0)
                {
                    int value = rnd.Next(1, 10);
                    SparseMatrixElements[index++] = new SparseMatrixElement(value, row, column);
                    quantity--;
                }
            }
        }
        private SparseMatrix(int rows, int columns, int quantity)
        {
            Rows = rows; Columns = columns;
            SparseMatrixElements = new SparseMatrixElement[quantity];
        }

        private int Rows { get; set; }
        private int Columns { get; set; }
        private SparseMatrixElement[] SparseMatrixElements { get; set; }
        
        private int TakeValueFromMatrix(int row, int column)
        {
            foreach (var element in SparseMatrixElements)
            {
                if (element == null) continue;
                if (element.Row == row && element.Column == column)
                    return element.Value;
            }

            return 0;
        }

        public void ShowSparseMatrix()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                    Console.Write("{0} ", TakeValueFromMatrix(i, j));
                Console.WriteLine();
            }
        }

        public SparseMatrix Append(SparseMatrix sparseMatrix)
        {
            SparseMatrixElement[] arrayOfSummeryElements= new SparseMatrixElement[Rows * sparseMatrix.Columns];
            int quantity = 0;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    var sum = TakeValueFromMatrix(i, j) + sparseMatrix.TakeValueFromMatrix(i, j);
                    if (sum != 0)
                    {
                        arrayOfSummeryElements[quantity] = new SparseMatrixElement(sum,i,j);
                        quantity++;
                    }
                }
            }

            SparseMatrix summarySparseMatrix = new SparseMatrix(Rows, Columns, quantity);
            for (int i = 0; i < quantity; i++)
            {
                summarySparseMatrix.SparseMatrixElements[i] = new SparseMatrixElement(arrayOfSummeryElements[i].Value,
                    arrayOfSummeryElements[i].Row, arrayOfSummeryElements[i].Column);
            }
            return summarySparseMatrix;
        }
        
        public SparseMatrix Multiply(SparseMatrix sparseMatrix)
        {
            SparseMatrixElement[] arrayOfSummeryElements = new SparseMatrixElement[Rows * sparseMatrix.Columns];
            int sum = 0, quantity = 0;
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < sparseMatrix.Columns; j++)
                {
                    for (int l = 0; l < sparseMatrix.Rows; l++)
                        sum += TakeValueFromMatrix(i, l) * sparseMatrix.TakeValueFromMatrix(l, j);
                    if (sum != 0)
                    {
                        arrayOfSummeryElements[quantity]=new SparseMatrixElement(sum,i,j);
                        quantity++;
                    }

                    sum = 0;
                }
            }

            SparseMatrix multiplySparseMatrix = new SparseMatrix(Rows, sparseMatrix.Columns, quantity);
            for (int i = 0; i < quantity; i++)
            {
                multiplySparseMatrix.SparseMatrixElements[i] = new SparseMatrixElement(arrayOfSummeryElements[i].Value,
                    arrayOfSummeryElements[i].Row, arrayOfSummeryElements[i].Column);
            }
            return multiplySparseMatrix;
        }
    }
}
