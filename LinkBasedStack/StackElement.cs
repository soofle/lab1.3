﻿namespace Lab1._3_LinkBasedStack
{
    public class StackElement
    {
        public StackElement(string value)
        {
            Value = value;
            Next = null;
        }

        public string Value { get; set; }
        public StackElement Next { get; set; }
    }
}
