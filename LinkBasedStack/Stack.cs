﻿using System;

namespace Lab1._3_LinkBasedStack
{
    public class LinkBasedStack
    {
        public LinkBasedStack()
        {
            Current = null;
            Count = 0;
        }

        private StackElement Current { get; set; }
        public int Count { get; private set; }

        public void Push(string info)
        {
            if (Current == null)
            {
                Current = new StackElement(info);
                Count++;
                return;
            }
            var tempStackElement = Current;
            Current = new StackElement(info)
            {
                Next = tempStackElement
            };
            Count++;
        }

        public string Pop()
        {
            if (Current == null)
                return "Стек пуст!";

            string info = Current.Value;
            Current = Current.Next;
            Count--;
            return info;
        }

        public string Peek()
        {
            if (Current == null)
                return "Стек пуст!!";
            return Current.Value;
        }

        public void ShowStack()
        {
            if (Current == null)
            {
                Console.WriteLine("Стек Пуст!!!!");
                return;
            }
            StackElement showingStackElement = Current;
            while (showingStackElement != null)
            {
                Console.Write(showingStackElement.Value + " ");
                showingStackElement = showingStackElement.Next;
            }
            Console.WriteLine();
        }
    }
}
