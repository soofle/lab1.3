﻿using System;
using Lab1._3_BinaryTree;
using Lab1._3_SparseMatrix;
using Lab1._3_LinkBasedStack;
using Lab1._3_InputDataValidator;

namespace Lab1._3_Menu
{
    public static class Menu
    {
        public static void Start()
        {
            bool end = false;
            while (end != true)
            {
                Console.WriteLine("Выберите задачу:\n" +
                    "1. Стек для хранения строк.\n" +
                    "7. Разреженная числовая матрица.\n" +
                    "10. Бинарное дерево поиска, хранящее строки.\n" +
                    "999. Выход.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("1");
                        Task_1_Menu();
                        Console.Clear();
                        break;
                    case "7":
                        Console.WriteLine("7");
                        Task_7_Menu();
                        Console.Clear();
                        break;
                    case "10":
                        Console.WriteLine("10");
                        Task_10_Menu();
                        Console.Clear();
                        break;
                    case "999":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите 1 или 7 или 10 или 999");
                        break;
                }
            }
        }

        private static void Task_1_Menu()
        {
            Console.Clear();
            Console.WriteLine("Добро пожаловать в Задачу 1 \"Стек для хранения строк\"");
            var stack = new LinkBasedStack();
            bool end = false;
            while (end != true)
            {
                Console.WriteLine("\nВыберите действие:\n" +
                    "1. Добавить элемент(Push).\n" +
                    "2. Достать элемент(Pop).\n" +
                    "3. Считать головной элемент(Peek).\n" +
                    "4. Вывести колличество элементов.\n" +
                    "5. Вывести все элементы на экран\n" +
                    "9. Вернуться в Главное меню.");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Введите \"Значение\" элемента");
                        stack.Push(Console.ReadLine());
                        break;
                    case "2":
                        Console.WriteLine(stack.Pop());
                        break;
                    case "3":
                        Console.WriteLine(stack.Peek());
                        break;
                    case "4":
                        Console.WriteLine("Количество элементов: {0}", stack.Count);
                        break;
                    case "5":
                        stack.ShowStack();
                        break;
                    case "9":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите верное значение");
                        break;
                }
            }

        }
        
        private static void Task_7_Menu()
        { 
            Console.Clear();
            Console.WriteLine("Добро пожаловать в Задачу 7 \"Разреженная числовая матрица\"");

            Random rnd = new Random(DateTime.Now.Millisecond);
            var inputDataValidator = new InputDataValidator();

            bool end = false;
            while (end != true)
            {

                Console.WriteLine("\nВыберите действие: \n" +
                    "1. Создать случайную разреженную матрицу размера n на m и разреженности s вывести её на экран \n" +
                    "2. Сложить две случайные разреженные матрицы размера n на m \n" +
                    "3. Перемножить две случайные разреженные матрицы размера n на k и k на m со своими разреженнастями\n" +
                    "9. Вернуться в Главное меню.");

                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("\n N-кол-во строк(целочисленное), M-кол-во столбцов(целочисленное)," +
                            " S-разреженность матрицы(дробное в десятичном виде)\n" +
                            "Введите три значения в формате: n m s");

                        string[] inputData = Console.ReadLine().Split();
                        if (!inputDataValidator.ValidateDataForSparseMatrix(inputData))
                        {
                            break;
                        }

                        int rows = Int32.Parse(inputData[0]);
                        int columns = Int32.Parse(inputData[1]);
                        double sparsity = Double.Parse(inputData[2]);
                        
                        var sparseMatrix = new SparseMatrix(rows, columns, sparsity, rnd);
                        sparseMatrix.ShowSparseMatrix();
                        break;
                    case "2":
                        Console.WriteLine("\n N-кол-во строк(целочисленное), M-кол-во столбцов(целочисленное)," +
                            " S-разреженность матрицы(дробное в десятичном виде)\n" +
                            "Введите три значения в формате: n m s");

                        inputData = Console.ReadLine().Split();
                        if (!inputDataValidator.ValidateDataForSparseMatrix(inputData))
                        {
                            break;
                        }

                        rows = Int32.Parse(inputData[0]);
                        columns = Int32.Parse(inputData[1]);
                        sparsity = Double.Parse(inputData[2]);

                        var rMatrix1 = new SparseMatrix(rows, columns, sparsity, rnd);
                        var rMatrix2 = new SparseMatrix(rows, columns, sparsity, rnd);
                        rMatrix1.ShowSparseMatrix();
                        Console.WriteLine("+");
                        rMatrix2.ShowSparseMatrix();
                        Console.WriteLine("=");
                        var rMatrix3 = rMatrix1.Append(rMatrix2);
                        rMatrix3.ShowSparseMatrix();
                        break;
                    case "3":
                        Console.WriteLine("\n N-кол-во строк(целочисленное), K-кол-во столбцов(целочисленное) первой матрицы,\n" +
                            " K-кол-во строк(целочисленное), M-кол-во столбцов(целочисленное) второй матрицы\n" +
                            "S1 - разреженность матрицы 1 и S2 - рязреженность матрицы 2\n" +
                            "Введите пять значений в формате: n k m s1 s2");

                        inputData = Console.ReadLine().Split();

                        if (!inputDataValidator.ValidateDataForMultyplySparseMatrix(inputData))
                        {
                            break;
                        }

                        rows = Int32.Parse(inputData[0]);
                        columns = Int32.Parse(inputData[1]);
                        int k = Int32.Parse(inputData[2]);
                        sparsity = Double.Parse(inputData[3]);
                        double sparsity2 = Double.Parse(inputData[4]);
                        
                        var rMatrix4 = new SparseMatrix(rows, k, sparsity, rnd);
                        var rMatrix5 = new SparseMatrix(k, columns, sparsity2, rnd);
                        rMatrix4.ShowSparseMatrix();
                        Console.WriteLine("*");
                        rMatrix5.ShowSparseMatrix();
                        Console.WriteLine("=");
                        var rMatrix6 = rMatrix4.Multiply(rMatrix5);
                        rMatrix6.ShowSparseMatrix();
                        break;
                    case "9":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите верное значение");
                        break;
                }
            }
        }
        
        private static void Task_10_Menu()
        {
            Console.Clear();
            Console.WriteLine("Добро пожаловать в Задачу 10 \"Бинарное дерево поиска, хранящее строки\"");
            var binTree = new BinaryTree();
            bool end = false;
            while (end != true)
            {
                Console.WriteLine("\nВыберите действие: \n" +
                    "1. Добавить узел в дерево \n" +
                    "2. Удалить узел из дерева \n" +
                    "3. Вывести дерево на экран \n" +
                    "4. Заполнить дерево из файла \n" +
                    "9. Вернуться в Главное меню");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Введите ключ и значение");
                        string[] inputDatas = Console.ReadLine().Split();
                        binTree.Insert(inputDatas[0], inputDatas[1]);
                        break;
                    case "2":
                        Console.WriteLine("Введите удаляемый ключ");
                        binTree.Remove(Console.ReadLine());
                        break;
                    case "3":
                        Console.WriteLine("Наше дерево:");
                        binTree.Traverse(binTree.Root);
                        break;
                    case "4":
                        binTree.FillTreeFromFile("TreeElements.txt");
                        break;
                    case "9":
                        end = true;
                        break;
                    default:
                        Console.WriteLine("Введите верное значение");
                        break;
                }
            }
        }
    }
}
