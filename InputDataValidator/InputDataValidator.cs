﻿using System;

namespace Lab1._3_InputDataValidator
{
    public class InputDataValidator
    {
        private bool IsValueInRangeForRowsOrColumns(int value)
        {
            return (value > 0 && value < 100);
        }

        private bool IsValueInRangeForSparsity(double value)
        {
            return (value > 0 && value < 1);
        }

        public bool ValidateDataForSparseMatrix(string[] argh)
        {
            try
            {
                bool result = true;

                if (Int32.TryParse(argh[0], out int rows))
                {
                    if (!IsValueInRangeForRowsOrColumns(rows))
                    {
                        Console.WriteLine("Количество строк (n) должно быть задано в диапазоне от 0 до 100");
                        result = false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение количества строк (n) должно быть числом");
                    result = false;
                }

                if (Int32.TryParse(argh[1], out int columns))
                {
                    if (!IsValueInRangeForRowsOrColumns(columns))
                    {
                        Console.WriteLine("Количество столбцов (m) должно быть задано в диапазоне от 0 до 100");
                        result= false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение количества (m) стобцов должно быть числом");
                    result = false;
                }

                if (Double.TryParse(argh[2], out double sparsity))
                {
                    if (!IsValueInRangeForSparsity(sparsity))
                    {
                        Console.WriteLine(
                            "Значение разреженности матрицы (s) должно быть задано в диапазоне от 0 до 1");
                        result= false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение разреженности матрицы (s) должно быть дробным числом. Например: 0,2");
                    result = false;
                }

                return result;

            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Введены не все значения");
                return false;
            }
        }

        public bool ValidateDataForMultyplySparseMatrix(string[] argh)
        {
            try
            {
                bool result = true;

                if (Int32.TryParse(argh[0], out int rows))
                {
                    if (!IsValueInRangeForRowsOrColumns(rows))
                    {
                        Console.WriteLine("Количество строк (n) должно быть задано в диапазоне от 0 до 100");
                        result = false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение количества строк (n) должно быть числом");
                    result = false;
                }

                if (Int32.TryParse(argh[1], out int columns1))
                {
                    if (!IsValueInRangeForRowsOrColumns(columns1))
                    {
                        Console.WriteLine("Количество столбцов (k) должно быть задано в диапазоне от 0 до 100");
                        result = false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение количества стобцов (k) должно быть числом");
                    result = false;
                }

                if (Int32.TryParse(argh[2], out int columns2))
                {
                    if (!IsValueInRangeForRowsOrColumns(columns2))
                    {
                        Console.WriteLine("Количество столбцов (m) должно быть задано в диапазоне от 0 до 100");
                        result= false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение количества стобцов (m) должно быть числом");
                    result = false;
                }

                if (Double.TryParse(argh[3], out double sparsity1))
                {
                    if (!IsValueInRangeForSparsity(sparsity1))
                    {
                        Console.WriteLine("Значение разреженности матрицы (s1) должно быть задано в диапазоне от 0 до 1");
                        result= false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение разреженности матрицы (s1) должно быть дробным числом. Например: 0,2");
                    result = false;
                }

                if (Double.TryParse(argh[4], out double sparsity2))
                {
                    if (!IsValueInRangeForSparsity(sparsity2))
                    {
                        Console.WriteLine("Значение разреженности матрицы (s2) должно быть задано в диапазоне от 0 до 1");
                        result= false;
                    }
                }
                else
                {
                    Console.WriteLine("Значение разреженности матрицы (s2) должно быть дробным числом. Например: 0,2");
                    result = false;
                }

                return result;

            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Введены не все значения");
                return false;
            }
        }
    }
}
