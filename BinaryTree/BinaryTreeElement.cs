﻿namespace Lab1._3_BinaryTree
{
    public class BinaryTreeElement
    {
        public BinaryTreeElement(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; set; }
        public string Value { get; set; }
        public BinaryTreeElement LSon { get; set; }
        public BinaryTreeElement RSon { get; set; }
    }
}
