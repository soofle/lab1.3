﻿using System;
using System.IO;

namespace Lab1._3_BinaryTree
{
    public class BinaryTree
    {
        public BinaryTreeElement Root { get; set; }

        public void Insert(string key, string value)
        {
            if (Root == null)
            {
                Root = new BinaryTreeElement(key, value);
                return;
            }

            InsertTo(Root, key, value);
        }

        private void InsertTo(BinaryTreeElement binaryTreeElement, string key, string value)
        {
            if (key.CompareTo(binaryTreeElement.Key) < 0)
            {
                if (binaryTreeElement.LSon == null)
                {
                    binaryTreeElement.LSon = new BinaryTreeElement(key, value);
                    return;
                }

                InsertTo(binaryTreeElement.LSon, key, value);
                return;
            }

            if (key.CompareTo(binaryTreeElement.Key) > 0)
            {
                if (binaryTreeElement.RSon == null)
                {
                    binaryTreeElement.RSon = new BinaryTreeElement(key, value);
                    return;
                }

                InsertTo(binaryTreeElement.RSon, key, value);
                return;
            }

            //если ключ существует - изменяем значение по ключу
            binaryTreeElement.Value = value;
        }

        public BinaryTreeElement Minimum(BinaryTreeElement binaryTreeElement)
        {
            if (binaryTreeElement.LSon == null)
                return binaryTreeElement;
            return Minimum(binaryTreeElement.LSon);
        }

        public void Remove(string key)
        {
            Root = RemoveFrom(Root, key);
        }

        private BinaryTreeElement RemoveFrom(BinaryTreeElement root, string key)
        {
            if (root == null)
                return null;

            if (key.CompareTo(root.Key) < 0)
                root.LSon = RemoveFrom(root.LSon, key);
            else
            {
                if (key.CompareTo(root.Key) > 0)
                    root.RSon = RemoveFrom(root.RSon, key);
                else
                {
                    if (root.RSon != null && root.LSon != null)
                    {
                        root.Value = Minimum(root.RSon).Value;
                        root.Key = Minimum(root.RSon).Key;
                        root.RSon = RemoveFrom(root.RSon, root.Key);
                    }
                    else
                    {
                        if (root.LSon != null)
                            root = root.LSon;
                        else
                            root = root.RSon;
                    }
                }
            }
            return root;
        }

        public void Traverse(BinaryTreeElement root)
        {
            if (root == null)
                return;

            if (root.LSon != null)
                Traverse(root.LSon);
            PrintNode(root);
            if (root.RSon != null)
                Traverse(root.RSon);
        }

        private void PrintNode(BinaryTreeElement binaryTreeElement)
        {
            Console.WriteLine("{0} - {1}", binaryTreeElement.Key, binaryTreeElement.Value);
        }

        public void FillTreeFromFile(string path)
        {
            var lines = File.ReadLines(path);
            foreach (var line in lines)
            {
                if (!string.IsNullOrWhiteSpace(line))
                {
                    var datas = line.Split();
                    Insert(datas[0], datas[1]);
                }
            }
        }
    }
}
